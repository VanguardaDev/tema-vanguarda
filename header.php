<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'vanguarda' ); ?></a>
	
	<header id="masthead" class='bg-dark py-3 d-none d-lg-block fixed-top mb-5'>
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-lg-3">
					<?php the_custom_logo(); ?>   
				</div>
				<div class="col-lg-8 col-xl-7">
					<div class="w-100">
						<ul id='head-text' class="list-inline text-white border-bottom pb-1 text-center">
							<li class="list-inline-item f-08">
								<i class="fas fa-phone-square mr-1"></i>
								+55 92 9 9888.7654
							</li>
							<li class="list-inline-item f-08">
								<i class="fas fa-envelope mr-1"></i>
								<a href="mailto:contato@servitech-am.com.br" target='_blank'>
									contato@servitech-am.com.br
								</a>
							</li>
							<li class="list-inline-item f-08">
								<i class="fas fa-clock mr-1"></i>
								Seg - Sáb 9h às 18h
							</li>
							<li class="list-inline-item f-08" style='margin-right: .2rem;'>
								<a href="" target='_blank'>
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<li class="list-inline-item f-08">
								<a href="" target='_blank'>
									<i class="fab fa-instagram"></i>
								</a>
							</li>
						</ul>
					</div>           
					<?php /* Primary navigation */
						wp_nav_menu( array(
						'menu' => 'top_menu',
						'depth' => 2,
						'container' => false,
						'menu_class' => 'nav',
						//Process nav menu using our custom nav walker
						'walker' => new wp_bootstrap_navwalker())
						);
					?>
				</div>
			</div>
		</div>		
	</header>
	<header class='d-md-block d-lg-none py-2 bg-dark'>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<?php the_custom_logo(); ?> 
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="nav-menu">
		<?php /* Primary navigation */
			wp_nav_menu( array(
			'menu' => 'top_menu',
			'depth' => 2,
			'container' => false,
			'menu_class' => 'nav',
			//Process nav menu using our custom nav walker
			'walker' => new wp_bootstrap_navwalker())
			);
		?>
        </div>
    </nav>
</header>
	<div id="content" class="site-content">
