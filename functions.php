<?php  require_once('wp_bootstrap_navwalker.php'); ?>
<?php

if ( ! function_exists( 'vanguarda_setup' ) ) :
	function vanguarda_setup() {
		load_theme_textdomain( 'vanguarda', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'vanguarda' ),
		) );
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'vanguarda_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'vanguarda_setup' );

function vanguarda_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'vanguarda_content_width', 640 );
}
add_action( 'after_setup_theme', 'vanguarda_content_width', 0 );

function vanguarda_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'vanguarda' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'vanguarda' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'vanguarda_widgets_init' );

function vanguarda_scripts() {
	wp_enqueue_style( 'vanguarda-style', get_stylesheet_uri() );

	wp_enqueue_script( 'vanguarda-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'vanguarda-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'vanguarda_scripts' );

require get_template_directory() . '/inc/custom-header.php';
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/template-functions.php';
require get_template_directory() . '/inc/customizer.php';
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function get_bootstrap() {
	wp_enqueue_style('bootstrap4CSS', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('styleCSS', get_template_directory_uri() . '/css/style.css');
	wp_enqueue_script('bootstrap4JS', get_template_directory_uri() .'/js/bootstrap.min.js', array('jquery'), '20151215', true);
}

add_action('wp_enqueue_scripts', 'get_bootstrap');