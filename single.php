<?php get_header(); ?>
	<div id="primary" class="container pb-5">
		<div id="main" class="row topi">
			<div class="col-xl-9" id="site-main">
			<?php
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				endwhile; // End of the loop.
			?>
			</div>
			<div class="col-xl-3">
				<div class="w-100 pb-4 m-top"><?php get_sidebar(); ?></div>
			</div>
		</div><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
