	</div><!-- #content -->
	<footer id="footer" class="py-3 bg-servitech">
		<div class="container">
			<p class="text-center mb-0 text-white">
				Todos os direitos reservados © SERVITECH | Feito 
				<a href="https://vanguardacomunicacao.com.br" target="_blank" style="color: #ff0000;">Vanguarda Comunicação</a>
			</p>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
